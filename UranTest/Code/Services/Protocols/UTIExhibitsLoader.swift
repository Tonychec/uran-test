//
//  UTIExhibitsLoader.swift
//  UranTest
//
//  Created by iosNomad on 10/31/18.
//  Copyright © 2018 iosNomad. All rights reserved.
//

import Foundation

protocol  UTIExhibitsLoaderProtocol: class {
    func getExhibitList(handler: ([UTExhibit]) -> Void)
}

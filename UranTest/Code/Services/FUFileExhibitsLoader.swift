//
//  FUFileExhibitsLoader.swift
//  UranTest
//
//  Created by iosNomad on 10/31/18.
//  Copyright © 2018 iosNomad. All rights reserved.
//

import UIKit

class UTFileExhibitsLoader: UTIExhibitsLoaderProtocol {
    
    func getExhibitList(handler: ([UTExhibit]) -> Void) {
        guard let path = Bundle.main.path(forResource: Keys.resourceName.rawValue, ofType: Keys.fileExt.rawValue) else {
            return
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let listJson = jsonResult[Keys.listJson.rawValue] as? [Dictionary<String, AnyObject>] {
                var items = [UTExhibit]()
                
                for listItem in listJson {
                    var images = [String]()
                    for image in (listItem[Keys.images.rawValue] as! [String]) {
                        images.append(image)
                    }
                    
                    items.append(UTExhibit(title: listItem[Keys.title.rawValue] as! String, images: images))
                }
                
                handler(items)
            }
        } catch {
            fatalError("UTFileExhibitsLoader_getExhibitList_fatal_error")
        }
    }
    
    
    enum Keys: String {
        case resourceName = "ios_challenge_json"
        case fileExt = "txt"
        case listJson = "list"
        case images = "images"
        case title = "title"
    }
}

//
//  UTMainViewController.swift
//  UranTest
//
//  Created by iosNomad on 10/31/18.
//  Copyright © 2018 iosNomad. All rights reserved.
//

import UIKit

class UTMainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var items = [UTExhibit]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        
        UTFileExhibitsLoader().getExhibitList(handler: { items in
            self.items = items
            tableView.reloadData()
        })
    }
    
    enum ReuseIdentifiers: String {
        case utExhibitTableViewCell = "UTExhibitTableViewCell"
    }
}


//MARK: - Configurations
extension UTMainViewController {

    func setupTableView() {
        tableView.dataSource = self
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: String(describing: UTExhibitTableViewCell.self), bundle: nil), forCellReuseIdentifier: ReuseIdentifiers.utExhibitTableViewCell.rawValue)
    }
}


//MARK: - UITableViewDataSource
extension UTMainViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.utExhibitTableViewCell.rawValue, for: indexPath) as! UTExhibitTableViewCell
        cell.fill(item: items[indexPath.row])
        
        return cell
    }
}

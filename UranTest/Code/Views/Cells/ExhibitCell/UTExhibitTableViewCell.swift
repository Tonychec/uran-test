//
//  UTExhibitTableViewCell.swift
//  UranTest
//
//  Created by iosNomad on 10/31/18.
//  Copyright © 2018 iosNomad. All rights reserved.
//

import UIKit

class UTExhibitTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    
    private var images = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCollectionView()
    }
    
    enum ReuseIdentifiers: String {
        case utImageCollectionViewCell = "UTImageCollectionViewCell"
    }
}


//MARK: - Configurations
extension UTExhibitTableViewCell {
    
    func fill(item: UTExhibit) {
        itemTitleLabel.text = item.title
        self.images = item.images
        collectionView.reloadData()
    }
    
    private func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: String(describing: UTImageCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: ReuseIdentifiers.utImageCollectionViewCell.rawValue)
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}


//MARK: - Configurations
extension UTExhibitTableViewCell:  UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseIdentifiers.utImageCollectionViewCell.rawValue, for: indexPath) as! UTImageCollectionViewCell
        cell.fill(imgUrlStr: images[indexPath.row])
        
        return cell
    }
}

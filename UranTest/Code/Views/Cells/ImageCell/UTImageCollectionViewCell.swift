//
//  UTImageCollectionViewCell.swift
//  UranTest
//
//  Created by iosNomad on 10/31/18.
//  Copyright © 2018 iosNomad. All rights reserved.
//

import UIKit

class UTImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
}


//MARK: - Configurations
extension UTImageCollectionViewCell {
    
    func fill(imgUrlStr: String) {
        imageView.downloaded(from: imgUrlStr)
    }
}

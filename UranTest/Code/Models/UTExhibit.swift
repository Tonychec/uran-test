//
//  UTExhibit.swift
//  UranTest
//
//  Created by iosNomad on 10/31/18.
//  Copyright © 2018 iosNomad. All rights reserved.
//

import Foundation

class UTExhibit {
    
    let title: String
    var images: [String]
    
    init(title: String, images: [String] = []) {
        self.title = title
        self.images = images
    }
}
